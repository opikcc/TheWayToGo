package main

import (
	"fmt"
	"reflect"

	i "interfacer/interfacePack"
)

func main() {
	sq1 := new(i.Square)
	sq1.Side = 5

	// var areaIntf Shaper
	// areaIntf = sq1
	// shorter, without separate declaration:
	// areaIntf := Shaper(sq1)
	// or even:
	areaIntf := sq1
	fmt.Printf("The square has area: %f\n", areaIntf.Area())

	r := i.Rectangle{5, 3} // Area() of Rectangle needs a value
	q := &i.Square{5}      // Area() of Square needs a pointer
	// shapes := []Shaper{Shaper(r), Shaper(q)}
	// or shorter:
	shapes := []i.Shaper{r, q}
	fmt.Println("Looping through shapes for area ...")
	for n, _ := range shapes {
		fmt.Println("Shape details: ", shapes[n])
		fmt.Println("Area of this shape is: ", shapes[n].Area())
	}

	var areaIface interface{} = q
	if t, ok := areaIface.(*i.Square); ok {
		fmt.Printf("The type of areaIface is %T\n", t)
	}

	if t, ok := areaIface.(i.Rectangle); ok {
		fmt.Printf("The type of areaIface is %T\n", t)
	}

	areaIface = 12.4
	switch t := areaIface.(type) {
	case *i.Square:
		fmt.Printf("Type Square %T with value %v\n", t, t)
	case i.Rectangle:
		fmt.Printf("Type Rectangle %T with value %v\n", t, t)
	case float64:
		fmt.Printf("Type Float64 %T with value %v\n", t, t)
	case nil:
		fmt.Println("nil value: nothing to check?")
	default:
		fmt.Printf("Unexpected type %T", t)
	}

	i.Classifier(1, 2.3, "Test")

	// A bare value
	var lst i.List

	// compiler error:
	// cannot use lst (type List) as type Appender in function argument:
	// List does not implement Appender (Append method requires pointer        // receiver)
	// i.CountInto(lst, 1, 10)
	if i.LongEnough(lst) { // VALID: Identical receiver type
		fmt.Printf("- lst is long enough\n")
	}

	// A pointer value
	plst := new(i.List)
	i.CountInto(plst, 1, 10) // VALID: Identical receiver type
	if i.LongEnough(plst) {
		// VALID: a *List can be dereferenced for the receiver
		fmt.Printf("- plst is long enough\n")
		// - plst2 is long enoug
	}

	// var x float64 = 3.4
	// fmt.Println("type:", reflect.TypeOf(x))
	// v := reflect.ValueOf(x)
	// fmt.Println("value:", v)
	// fmt.Println("type:", v.Type())
	// fmt.Println("kind:", v.Kind())
	// fmt.Println("value:", v.Float())
	// fmt.Println(v.Interface())
	// fmt.Printf("value is %5.2e\n", v.Interface())
	// y := v.Interface().(float64)
	// fmt.Println(y)

	var x float64 = 3.4
	v := reflect.ValueOf(x)
	// setting a value:
	// Error: will panic: reflect.Value.SetFloat using unaddressable value
	// v.SetFloat(3.1415)
	fmt.Println("settability of v:", v.CanSet())
	v = reflect.ValueOf(&x) // Note: take the address of x.
	fmt.Println("type of v:", v.Type())
	fmt.Println("settability of v:", v.CanSet())
	v = v.Elem()
	fmt.Println("The Elem of v is: ", v)
	fmt.Println("settability of v:", v.CanSet())
	v.SetFloat(3.1415) // this works!
	fmt.Println(v.Interface())
	fmt.Println(v)
}
