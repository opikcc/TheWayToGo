package structPack

type TwoInts struct {
	A int
	B int
}

func (tn *TwoInts) AddThem() int {
	return tn.A + tn.B
}

func (tn *TwoInts) AddToParam(param int) int {
	return tn.A + tn.B + param
}
