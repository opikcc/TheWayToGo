package readings

import (
	"bufio"
	"fmt"
	"io"
	"io/ioutil"
	"os"
)

func ReadInput() {
	var (
		firstName, lastName, s string
		i                      int
		f                      float32
		input                  = "56.12 / 5212 / Go"
		format                 = "%f / %d / %s"
	)

	fmt.Println("Please enter your full name: ")
	fmt.Scanln(&firstName, &lastName)
	// fmt.Scanf("%s %s", &firstName, &lastName)
	fmt.Printf("Hi %s %s!\n", firstName, lastName) // Hi Chris Naegels
	fmt.Sscanf(input, format, &f, &i, &s)
	fmt.Println("From the string we read: ", f, i, s)
	// ouwtput: From the string we read: 56.12 5212 Go
}

func BuffIOReadInput() {
	var (
		inputReader *bufio.Reader
		input       string
		err         error
	)

	inputReader = bufio.NewReader(os.Stdin)
	fmt.Println("Please enter some input: ")
	input, err = inputReader.ReadString('\n')

	if err == nil {
		fmt.Printf("The input was: %s\n", input)
	}
}

func ReadWriteToFile() {
	inputFile, inputError := os.Open("input.dat")
	if inputError != nil {
		fmt.Printf("An error occured on opening the inputFile\n" + "Does the file exist?\n" + "Have you got access to it?\n")
		return // exit the function on error
	}
	defer inputFile.Close()

	inputReader := bufio.NewReader(inputFile)
	for {
		inputString, readerError := inputReader.ReadString('\n')
		if readerError == io.EOF {
			return
		}

		fmt.Printf("The input was: %s", inputString)
	}
}

func ReadWriteIOUtil() {
	inputFile := "input.dat"
	outputFile := "input_copy.dat"
	buf, err := ioutil.ReadFile(inputFile)
	if err != nil {
		fmt.Fprintf(os.Stderr, "File Error: %s\n", err)
		// panic(err.Error())
	}

	fmt.Printf("%s\n", string(buf))
	err = ioutil.WriteFile(outputFile, buf, 0x644)
	if err != nil {
		panic(err.Error())
	}
}
