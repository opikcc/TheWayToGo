package networking

import (
	"bufio"
	"fmt"
	"io"
	"log"
	"net"
	"net/http"
	"os"
	"strings"
)

func doServerStuff(conn net.Conn) {
	for {
		buf := make([]byte, 512)
		_, err := conn.Read(buf)
		if err != nil {
			fmt.Println("Error Reading", err.Error())
			return // Terminate Program
		}
		fmt.Printf("Received data : %v", string(buf))
	}
}

func SimpleServer() {
	fmt.Println("Starting the server ...")
	// Create Listener:
	listener, err := net.Listen("tcp", "localhost:50000")
	if err != nil {
		fmt.Println("Error Listening", err.Error())
		return // Terminate Program
	}
	// Listen and Accept Connection From Client
	for {
		conn, err := listener.Accept()
		if err != nil {
			fmt.Println("Error Accepting", err.Error())
			return // Terminate Program
		}
		go doServerStuff(conn)
	}
}

func SimpleClient() {
	// Open Connection
	conn, err := net.Dial("tcp", "localhost:50000")
	if err != nil {
		// No connection could be made because the target machine actively refused it.
		fmt.Println("Error dialing", err.Error())
		return // terminate program
	}

	inputReader := bufio.NewReader(os.Stdin)
	fmt.Println("First, what is your name?")
	clientName, _ := inputReader.ReadString('\n')
	// fmt.Printf("CLIENTNAME %s",clientName)
	trimmedClient := strings.Trim(clientName, "\r\n")
	// "\r\n" on Windows,"\n" on Linux
	// send info to server until Quit:
	for {
		fmt.Println("What to send to the server? Type Q to quit.")
		input, _ := inputReader.ReadString('\n')
		trimmedInput := strings.Trim(input, "\r\n")
		// fmt.Printf("input:--%s--",input)
		// fmt.Printf("trimmedInput:--%s--",trimmedInput)
		if trimmedInput == "Q" {
			return
		}
		_, err = conn.Write([]byte(trimmedClient + " says: " + trimmedInput))
	}
}

func SocketIO() {
	var (
		host          = "www.apache.org"
		port          = "80"
		remote        = host + ":" + port
		msg    string = "GET / \n"
		data          = make([]uint8, 4096)

		read  = true
		count = 0
	)

	// create the socket
	con, err := net.Dial("tcp", remote)

	// send our message.  an HTTP GET request in this case
	io.WriteString(con, msg)

	// read the response from the webserver
	for read {
		count, err = con.Read(data)
		read = (err == nil)
		fmt.Printf(string(data[0:count]))
	}
	con.Close()
}

func HelloServer(w http.ResponseWriter, req *http.Request) {
	fmt.Println("Inside HelloServer handler")
	fmt.Fprint(w, "Hello, "+req.URL.Path[1:])
}

func HelloWebServer() {
	http.HandleFunc("/", HelloServer)
	fmt.Println("Starting the server ...")
	err := http.ListenAndServe("localhost:9000", nil)
	if err != nil {
		log.Fatal("ListenAndServe: ", err.Error())
	}

}
