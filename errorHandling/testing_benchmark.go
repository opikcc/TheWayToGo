package errorHandling

import (
	"flag"
	"fmt"
	"log"
	"os"
	"runtime/pprof"
)

func Even(i int) bool { // Exported functions
	return i%2 == 0
}

func Odd(i int) bool {
	return i%2 != 0
}

func EvenOddTest() {
	var cpuprofile = flag.String("cpuprofile", "", "write cpu profile to file")

	for i := 0; i <= 100; i++ {
		fmt.Printf("Is the integer %d even? %v\n", i, Even(i))
	}

	flag.Parse()
	if *cpuprofile != "" {
		f, err := os.Create(*cpuprofile)
		if err != nil {
			log.Fatal(err)
		}
		pprof.StartCPUProfile(f)
		defer pprof.StopCPUProfile()
	}

}
