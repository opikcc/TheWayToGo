package errorHandling

import "testing"
import "runtime/pprof"

func TestEven(t *testing.T) {
	if Even(10) {
		t.Log("Everything OK: 10 is even, just a test to see failed output!")
		t.Fail()
	}
	if Even(7) {
		t.Log("7 is not even!")
		t.Fail()
	}
}
func TestOdd(t *testing.T) {
	if !Odd(11) {
		t.Log("11 must be odd!")
		t.Fail()
	}
	if Odd(10) {
		t.Log("10 is not odd!")
		t.Fail()
	}
}

var tests = []struct{  // Test table
	in  string
	out string
}{
	{"in1", "exp1"},
	{"in2", "exp2"},
	{"in3", "exp3"},
}

func TestFunction(t *testing.T) {
	for i, tt := range tests {
		s := FuncToBeTested(tt.in)
		verify(t, i, "FuncToBeTested: ", tt.in, s, tt.out)
	}
}

func verify(t *testing.T, testnum int, testcase, input, output, expected string)
{
  if input != output {
   t.Errorf("%d. %s with input = %s: output %s != %s", testnum,
   testcase, input, output, expected)
  }
}
