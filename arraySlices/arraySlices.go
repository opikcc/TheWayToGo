package arraySlices

import "fmt"

func ArrayInt() {
	var arr [5]int

	for i := 0; i < 5; i++ {
		arr[i] = i * 2
	}

	for i := 0; i < 5; i++ {
		fmt.Printf("arr %d : %d \n", i, arr[i])
	}

	for i := range arr {
		fmt.Printf("arr %d : %d \n", i, arr[i])
	}
}

func ArrayInit() {
	var a = [5]int{1, 2, 3}
	fmt.Println("a : ", a)

	// Lazy
	var b = [...]int{1, 2}
	fmt.Println("b : ", b)

	// Slice
	var c = new([3]int)
	c[0] = 1
	fmt.Println("c : ", c)

	// Slice
	var d = [...]int{1, 2}
	d[0] = 0
	fmt.Println("d : ", d)

	var e = []int{3, 2, 1}
	fmt.Println("e : ", e)

	s := []int{1, 2, 3}
	fmt.Println("s : ", s)
}

func ArrayNew() {
	var x = new([10]int)
	x1 := x[0:5]
	fmt.Println("x : ", x)
	fmt.Println("x1 : ", x1)

	var y []int = make([]int, 10)
	y1 := y[0:5]
	fmt.Println("y : ", y)
	fmt.Println("y1 : ", y1)
}

func Reslicing() {
	slice1 := make([]int, 0, 10)
	// load the slice, cap(slice1) is 10:
	for i := 0; i < len(slice1); i++ {
		slice1 = slice1[0 : i+1] // reslice
		slice1[i] = i
		fmt.Printf("The length of slice is %d\n", len(slice1))
	}

	// print the slice:
	for i := 0; i < len(slice1); i++ {
		fmt.Printf("Slice at %d is %d\n", i, slice1[i])
	}
}

func CopyingSlice() {
	sl_from := []int{1, 2, 3}
	sl_to := make([]int, 10)
	n := copy(sl_to, sl_from)
	fmt.Println(sl_to)                    // output: [1 2 3 0 0 0 0 0 0 0]
	fmt.Printf("Copied %d elements\n", n) // n == 3
	sl3 := []int{1, 2, 3}
	sl3 = append(sl3, 4, 5, 6)
	fmt.Println(sl3) // output: [1 2 3 4 5 6]
}

// func AppendByte(slice []byte, data ...byte) []byte {
// 	m := len(slice)
// 	n := m + len(data)
// 	if n > cap(slice) {
// 		// if necessary, reallocate
// 		// allocate double what’s needed, for future growth.
// 		newSlice := make([]byte, (n+1)*2)
// 		copy(newSlice, slice)
// 		slice = newSlice
// 	}
// 	slice = slice[0:n]
// 	copy(slice[m:n], data)
// 	return slice
// }

// func copy(dst, src []byte) int

func ForString() {
	var arrayOfBytes []byte
	var arrayOfRunes []rune
	s := "\u00ff\u754c"
	for i, c := range s {
		fmt.Printf("%d:%c ", i, c)
	}
	fmt.Println()
	fmt.Println("Length : ", len(s))

	arrayOfBytes = []byte(s)
	fmt.Println(arrayOfBytes)

	for i, c := range arrayOfBytes {
		fmt.Printf("Index : %d, Character : %c \n", i, c)
	}

	arrayOfRunes = []rune(s)
	fmt.Println(arrayOfRunes)

	for i, c := range arrayOfRunes {
		fmt.Printf("Index : %d, Character : %c \n", i, c)
	}

	stringSlice := append([]byte(s), 12)

	for i, c := range stringSlice {
		fmt.Printf("Index : %d, Character : %c \n", i, c)
	}
}
