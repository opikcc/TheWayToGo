package goroutines

import (
	"fmt"
	"time"
)

func sendData(ch chan string) {
	ch <- "Washington"
	ch <- "Tripoli"
	ch <- "London"
	ch <- "Beijing"
	ch <- "Tokyo"
}

func getData(ch chan string) {
	var input string
	for {
		input = <-ch
		fmt.Printf("%s \n", input)
	}
}

func Channeling() {
	ch := make(chan string)
	go sendData(ch)
	go getData(ch)
	time.Sleep(1e9)
}

func PumpingChannel() {
	pump := make(chan int)
	f := func(ch chan int) {
		for i := 1; i < 1e9; i++ {
			ch <- i
		}
	}

	go f(pump)

	go func(ch chan int) {
		for {
			fmt.Println("Channel : ", <-ch)
		}
	}(pump)

	time.Sleep(3e9)
}

func SemaphoreChannel() {
	done := make(chan int)

	go func() {
		i := 1
		for {
			fmt.Println("i : ", i)
			i++
			if i > 1e4 {
				done <- i
				break
			}
		}
	}()

	fmt.Println("Done : ", <-done)
}

func pump() chan int {
	ch := make(chan int)
	go func() {
		for i := 0; ; i++ {
			ch <- i
		}
	}()
	return ch
}

func suck(ch chan int) {
	for {
		fmt.Println(<-ch)
	}
}

func ChannelIdiom() {
	stream := pump()
	go suck(stream)
	// the above 2 lines can be shortened to: go suck( pump() )
	time.Sleep(1e9)
}
