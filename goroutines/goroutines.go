package goroutines

import (
	"flag"
	"fmt"
	"log"
	"runtime"
	"time"
)

func longWait() {
	fmt.Println("Beginning longWait()")
	time.Sleep(5 * 1e9) // sleep for 5 seconds
	fmt.Println("End of longWait()")
}

func shortWait() {
	fmt.Println("Beginning shortWait()")
	time.Sleep(2 * 1e9) // sleep for 2 seconds
	fmt.Println("End of shortWait()")
}

func SetGoMaxProcs() {
	start := time.Now()
	var numCores = flag.Int("n", 2, "number of CPU cores to use")
	flag.Parse()
	runtime.GOMAXPROCS(*numCores)

	fmt.Println("In main()")
	go longWait()
	go shortWait()
	fmt.Println("About to sleep in main()")
	// sleep works with a Duration in nanoseconds (ns) !
	time.Sleep(10 * 1e9)
	fmt.Println("At the end of main()")

	elapsed := time.Since(start)
	log.Printf("Exchange Sort took %s", elapsed)
}

func ConcurrentProcess() {
	var counter int
	start := time.Now()

	go func() {
		for i := 0; i < 1e9; i++ {
			fmt.Println("i : ", counter)
			counter++
		}
	}()

	go func() {
		for x := 0; x < 1e9; x++ {
			fmt.Println("x : ", counter)
			counter++
		}
	}()

	time.Sleep(3e9)

	elapsed := time.Since(start)
	log.Printf("Function took %s", elapsed)
}
